# CFPs

## Workflow

1. [Create a new CFP issue](https://gitlab.com/everyonecancontribute/devrel/learn-julia/cfps/-/issues/new) and collect all details as Single-Source-of-Truth 
1. Prepare talk submissions and/or create new talk ideas 
1. Update issues with acceptance state, and learnings at the event 
