<!-- Hey, Thank you for taking the time to create an issue for your talk, this helps us coordinate submissions and have a central place for submissions to a CFP. Helpful resources: https://about.gitlab.com/handbook/marketing/corporate-marketing/speaking-resources/-->

<!-- Please use the format below to add an Issue title. Dates should be using ISO dates, see https://about.gitlab.com/handbook/communication/#writing-style-guidelines -->

<!-- CFP: <Event Name>, <Location>, <Event Dates> - Due: <CFP Due Date> -->

## Event Details

* `Name of Event`: 
* `Event Website`:
* `Event Dates`: 
* `Location`: 
* `CFP Closes`: 
* `CFP Notification Date`:  
* `CFP Submission Link`: 
* `GitLab Event Issue/Epic`: <!-- Link to Developer Evangelism Issue if available -->


## Event Requirements

Follow the GitLab guidelines for speaker diversity. 

* [ ] Review and acknowledge the [event requirements](https://about.gitlab.com/handbook/marketing/corporate-marketing/speaking-resources/#event-requirements) for speakers
  * [ ] Assign yourself and set the due date to CfP notifications.
  * [ ] Review the speakers lineup for event requirements, when announced.

## Co-ordination

### Submissions


| Speaker(s)  | Topic   | Link to submission draft (where available) | Submission Status |
|-------------|---------|--------------------------------------------|-------------------|
|             |         |                                            |                   |
|             |         |                                            |                   |
|             |         |                                            |                   |


## Checklist

* [ ] Discuss submission ideas with @dnsmichi 
* [ ] Prepare abstract Google docs including title, abstract, benefits for the ecosystem, learning goals, etc. 
* [ ] Prepare Bio/CV doc
* [ ] Add `CFP::Accepted` and `CFP-Accepted::1` labels when accepted
* [ ] Promote speaker's cards on social media when allowed to share


FYI: @dnsmichi 

/assign @juliafmorgado <!-- Assign your self to issue -->

<!-- Please leave the label below on this issue https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/workflow/#cfp-workflow -->

/label ~"CFP::Open" 

<!-- Set a due date either until the CFP end date, or the event date using: /due April 17 -->

